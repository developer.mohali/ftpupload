﻿
function submitCloseSiteForm(formId) {
    var form = document.getElementById(formId);
    var successMessage = form.getAttribute("data-success");
    var failMessage = form.getAttribute("data-fail");
    var waitMessage = form.getAttribute("data-wait");
    var data = serialize(form) + "&emailSubject=" + form.getAttribute("data-email-subject") + "&emailfrom=" + form.getAttribute("data-email-from") + "&emailtemplate=" + form.getAttribute("data-template") + "&emailsignatur=" + form.getAttribute("data-signatur-markup") + "&emailintro=" + form.getAttribute("data-intro-text-markup");

    //Add overlay
    var overlay = document.createElement('div');
    overlay.className = "form-overlay";
    overlay.innerHTML = waitMessage;
    form.appendChild(overlay);


    //Now send to Umbraco....    
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open('POST', '/umbraco/api/Formular/sumbitCloneSiteFormular', true);
    xmlhttp.setRequestHeader('Content-Type', 'application/json');

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4) {
            if (xmlhttp.status === 200) {
                overlay.innerHTML = successMessage;
                //console.log(xmlhttp);
                console.log(xmlhttp.responseText);
            }
            else {
                overlay.innerHTML = failMessage;
            }
        }
    };
    xmlhttp.send(JSON.stringify(data));

}


