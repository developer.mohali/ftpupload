﻿angular.module("umbraco")
    .controller("Umbraco.PropertyEditors.Grid.VideoMediaController",
    function ($scope, $timeout, userService, editorService) {
        
        
        $scope.thumbnailUrl = getThumbnailUrl();
        
        
        if (!$scope.model.config.startNodeId) {
            if ($scope.model.config.ignoreUserStartNodes === true) {
                $scope.model.config.startNodeId = -1;
                $scope.model.config.startNodeIsVirtual = true;

            } else {
                userService.getCurrentUser().then(function (userData) {
                    $scope.model.config.startNodeId = userData.startMediaIds.length !== 1 ? -1 : userData.startMediaIds[0];
                    $scope.model.config.startNodeIsVirtual = userData.startMediaIds.length !== 1;
                });
            }
        }
        
        $scope.setImage = function(){
            var startNodeId = $scope.model.config && $scope.model.config.startNodeId ? $scope.model.config.startNodeId : undefined;
            var startNodeIsVirtual = startNodeId ? $scope.model.config.startNodeIsVirtual : undefined;
            var value = $scope.control.value;
            var target = value
                ? {
                    udi: value.udi,
                    url: value.image,
                    image: value.image
                }
                : null;
            var mediaPicker = {
                startNodeId: startNodeId,
                startNodeIsVirtual: startNodeIsVirtual,
                
                showDetails: false,
                disableFolderSelect: true,
                onlyImages: false,
                dataTypeKey: $scope.model.dataTypeKey,
                currentTarget: target,
                submit: function(model) {
                    var selectedImage = model.selection[0];
                   
                    $scope.control.value = {
                       
                        id: selectedImage.id,
                        udi: selectedImage.udi,
                        image: selectedImage.image
                    };
                    
                    editorService.close();
                },
                close: function() {
                    editorService.close();
                }
            }
            
            editorService.mediaPicker(mediaPicker);
        };
        
        $scope.$watch('control.value', function(newValue, oldValue) {
            if(angular.equals(newValue, oldValue)){
                return; // simply skip that
            }
            
            $scope.thumbnailUrl = getThumbnailUrl();
        }, true);
        
        function getThumbnailUrl() {

            if($scope.control.value && $scope.control.value.image) {
                var url = $scope.control.value.image;

                return url;
            }
            
            return null;
        };

});