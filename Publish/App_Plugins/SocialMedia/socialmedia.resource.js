﻿// adds the resource to umbraco.resources module:
angular.module('umbraco.resources').factory('socialMediaResource',
    function ($q, $http, umbRequestHelper) {
        return {
            getSocialMediaSetting: function () {
                return umbRequestHelper.resourcePromise(
                    $http.get('backoffice/SocialMedia/SocialMediaSettingsApi/GetSocialMediaSetting'),
                    "Failed to save data");
            },
            getInstagramLoginLink: function () {
                return umbRequestHelper.resourcePromise(
                    $http.get('backoffice/SocialMedia/SocialMediaSettingsApi/GetInstagramLoginLink'),
                    "Failed to save data");
            },
            getFacebookLoginLink: function () {
                return umbRequestHelper.resourcePromise(
                    $http.get('backoffice/SocialMedia/SocialMediaSettingsApi/GetFacebookLoginLink'),
                    "Failed to save data");
            }
        };
    }
);