﻿angular.module("umbraco").controller("CustomWelcomeDashboardController", function ($scope, userService, logResource, welcomeResource ) {
    var vm = this;
    vm.UserName = "guest";
    vm.UserLogHistory = [];
    vm.people = [];
    var userLogOptions = {
        pageSize: 10,
        pageNumber: 1,
        orderDirection: "Descending",
        sinceDate: new Date(2020, 1, 1)
    };
    var user = userService.getCurrentUser().then(function (user) {
        vm.UserName = user.name;
    });
    welcomeResource.getAll().then(function (response) {
        vm.people = response;
    });
    logResource.getPagedUserLog(userLogOptions)
        .then(function (response) {
            vm.UserLogHistory = response;
            
        });
});
